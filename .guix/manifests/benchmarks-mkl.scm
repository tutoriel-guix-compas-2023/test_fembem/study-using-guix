(use-modules (guix transformations))

(define switch-blas
  (options->transformation
    '((with-input . "openblas=mkl"))))

(packages->manifest
  (list (switch-blas (specification->package "test_FEMBEM"))
        (switch-blas (specification->package "openmpi"))
        (switch-blas (specification->package "openssh"))
        (switch-blas (specification->package "sed"))
        (switch-blas (specification->package "which"))
        (switch-blas (specification->package "grep"))
        (switch-blas (specification->package "coreutils"))
        (switch-blas (specification->package "bash"))))
