(specifications->manifest
 (list "texlive-base"
       "texlive-hyperref"
       "texlive-latex-geometry"
       "texlive-fonts-ec"
       "texlive-latex-float"
       "texlive-graphics"
       "texlive-bibtex"
       "sed"
       "r"
       "r-ggplot2"
       "inkscape"
       "bash"
       "coreutils"))
