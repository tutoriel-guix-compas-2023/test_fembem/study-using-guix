(list
 (channel
  ;; Default Guix channel
  (name 'guix)
  (url "https://git.savannah.gnu.org/git/guix.git")
  (branch "master")
  (commit "7f3c6d3b3ba86a8051e394e4ec9a6f6089753cb1"))
 (channel
  ;; Channel providing packages for HPC scientific computing
  ;; => required for HMAT-OSS, Chameleon and test_FEMBEM
  (name 'guix-hpc)
  (url "https://gitlab.inria.fr/guix-hpc/guix-hpc.git")
  (branch "master")
  (commit "356a3b4200a5c7a15cf85fc0e8680b74444c689d"))
 (channel
  ;; Channel providing non-free packages for HPC scientific computing
  ;; => required for Intel(R) MKL
  (name 'guix-hpc-non-free)
  (url "https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free.git")
  (branch "master")
  (commit "e9b113ddadc69fd21026777f5f893ba8ae3183aa")))
