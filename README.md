# test_FEMBEM: Study using Guix

[![pipeline
status](https://gitlab.inria.fr/tutoriel-guix-compas-2023/test_fembem/study-using-guix/badges/master/pipeline.svg)](https://gitlab.inria.fr/tutoriel-guix-compas-2023/test_fembem/study-using-guix/-/commits/master)

An experimental study relying on the open-source edition of the `test_FEMBEM`
solver [3] linked to the Chameleon [4] and HMAT-OSS [5] matrix libraries.

[View
PDF](https://tutoriel-guix-compas-2023.gitlabpages.inria.fr/test_fembem/study-using-guix/study.pdf)

## Reproducing guidelines

The benchmarks the study is based on were carried on a laptop equipped with an
dodeca-core (16 threads) Intel(R) Core(TM) i5-12500H processor of 12th
generation clocked at 4.5GHz (Hyper-Threading enabled) [10], 16 GB of RAM and
running *Debian GNU/Linux 11*.

This section further provides instructions for reproducing the required software
environment, running the experiments, post-processing the results and producing
the final PDF document.

Note that the software environment of the study is managed using the Guix
package manager [1]. It allows for creating self-contained, executable
descriptions of entire software environments that can be easily modified and
reproduced.

Here, we assume that the target machine for reproducing the study runs Guix
natively and rely on the combination of =guix time-machine= and =guix shell=
commands to enter the experimental software environment. However, if the machine
misses Guix or the latter can not be installed there easily, e.g. on a
high-performance computing platform, it is possible to create a Docker [11]
bundle, a Singularity [12] image or a relocatable standalone tarball containing
the required software environment using Guix on another computer, i.e. a
personal laptop, and transfer the result to the target machine [2].

### Preparation

At first, let us clone this repository and navigate to the root of the latter.

```shell
git clone \
  https://gitlab.inria.fr/tutoriel-guix-compas-2023/test_fembem/study-using-guix.git
cd study-using-guix
```

### Performing experiments

At this point, we can run the experiments using the dedicated `run.sh` shell
script. The parameters for the different test cases that shall be run are
defined in `./benchmarks/definitions.csv`. See the comments in
`./benchmarks/run.sh` for more details about this file.

Note that we will run the experiments twice. The first time we will rely on the
`test_FEMBEM`, Chameleon and HMAT-OSS built using OpenBLAS [6], the open-source
implementation of BLAS [8] and LAPACK [9] routines,

```shell
guix time-machine -C .guix/channels.scm -- shell --pure \
    -m .guix/manifests/benchmarks-openblas.scm -- \
    bash ./benchmarks/run.sh -d ./benchmarks/definitions.csv \
    -o ./benchmarks/results-openblas
```

and the second time we will consider the vendor-specific Intel(R) MKL [7]
implementation. See discussion in `study.tex` for more details.

```shell
guix time-machine -C .guix/channels.scm -- shell --pure \
    -m .guix/manifests/benchmarks-mkl.scm -- \
    bash ./benchmarks/run.sh -d ./benchmarks/definitions.csv \
    -o ./benchmarks/results-mkl
```

### Post-processing

Once the benchmarks have finished running, we can post-processes the results in
`./benchmarks/results-openblas/results.csv` and
`./benchmarks/results-mkl/results.csv` using the `plot.R` R script.

We begin by generating the figures from within the root of the repository like
so.

```shell
guix time-machine -C .guix/channels.scm -- shell --pure \
    -m .guix/manifests/post-processing.scm -- Rscript plot.R \
    ./benchmarks/results-openblas/results.csv \
    ./benchmarks/results-mkl/results.csv
```

Finally, we produce the PDF document of the study using the command line below.

```shell
guix time-machine -C .guix/channels.scm -- shell --pure \
    -m .guix/manifests/post-processing.scm -- \
    latexmk --shell-escape -f -pdf -bibtex -interaction=nonstopmode study
```

Et voilà !

## References

1. GNU Guix software distribution and transactional package manager
   [https://guix.gnu.org/](https://guix.gnu.org/).
2. Invoking guix pack (GNU Guix Reference Manual)
   [https://guix.gnu.org/manual/en/html_node/Invoking-guix-pack.html](https://guix.gnu.org/manual/en/html_node/Invoking-guix-pack.html).
3. test_FEMBEM: A simple application for testing dense and sparse solvers with
   pseudo-FEM or pseudo-BEM matrices
   [https://gitlab.inria.fr/solverstack/test_fembem](https://gitlab.inria.fr/solverstack/test_fembem).
4. Chameleon: A dense linear algebra software for heterogeneous architectures
   [https://solverstack.gitlabpages.inria.fr/chameleon/](https://solverstack.gitlabpages.inria.fr/chameleon/).
5. HMAT-OSS: A hierarchical matrix library
   [https://github.com/jeromerobert/hmat-oss](https://github.com/jeromerobert/hmat-oss).
6. OpenBLAS: An optimized BLAS library
   [http://www.openblas.net/](http://www.openblas.net/).
7. Intel(R) Math Kernel Library: Intel(R)-Optimized Math Library for Numerical
   Computing
   [https://www.intel.com/content/www/us/en/developer/tools/oneapi/onemkl.html](https://www.intel.com/content/www/us/en/developer/tools/oneapi/onemkl.html).
8. BLAS (Basic Linear Algebra Subprograms)
   [https://www.netlib.org/blas/](https://www.netlib.org/blas/).
9. LAPACK — Linear Algebra PACKage
   [https://www.netlib.org/lapack/](https://www.netlib.org/lapack/).
10. Intel(R) Core(TM) i5-12500H Processor Product Specification
    [https://ark.intel.com/content/www/us/en/ark/products/96141/intel-core-i512500h-processor-18m-cache-up-to-4-50-ghz.html](https://ark.intel.com/content/www/us/en/ark/products/96141/intel-core-i512500h-processor-18m-cache-up-to-4-50-ghz.html).
11. Docker: Accelerated, Containerized Application Development
    [https://www.docker.com/](https://www.docker.com/).
12. Singularity Container Technology and Services
    [https://sylabs.io/](https://sylabs.io/).
